package com.turkcell.bipsaac.datarain.model;

public class POIIconRequest {
	private String type;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "POIIconRequest [type=" + type + "]";
	}

}
