package com.turkcell.bipsaac.datarain.model;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class POIIconData {
	private List<POIIconModel> icons;
	private List<POIIconModel> thumbnails;

	public POIIconData() {
		icons = new ArrayList<>();
		thumbnails = new ArrayList<>();
		JSONParser parser = new JSONParser();

		try {
			JSONArray jsonArray = (JSONArray) parser.parse(new FileReader("/home/uurysr/Documents/Temp/poi-picture.json"));

			for (Object o : jsonArray) {
				POIIconModel image = new POIIconModel();
				JSONObject poiIcon = (JSONObject) o;
				
				String type = (String) poiIcon.get("type");
				String company = (String) poiIcon.get("company");
				String name = (String) poiIcon.get("name");
				String url = (String) poiIcon.get("url");
				
				image.setCompany(company);
				image.setName(name);
				image.setType(type);
				image.setUrl(url);
				
				if ("POI Icon".equals(type)) {
					icons.add(image);
				} else if ("POI Thumbnail".equals(type)) {
					thumbnails.add(image);
				}
				
			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public List<POIIconModel> getIcons() {
		return icons;
	}

	public void setIcons(List<POIIconModel> icons) {
		this.icons = icons;
	}

	public List<POIIconModel> getThumbnails() {
		return thumbnails;
	}

	public void setThumbnails(List<POIIconModel> thumbnails) {
		this.thumbnails = thumbnails;
	}
}
