package com.turkcell.bipsaac.datarain.model;

public class POIIconModel {
	private String company;
	private String name;
	private String type;
	private String url;

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		return "POIIconModel [company=" + company + ", name=" + name + ", type=" + type + ", url=" + url + "]";
	}

}
