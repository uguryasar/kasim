package hello;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.turkcell.bipsaac.datarain.model.LoginInput;
import com.turkcell.bipsaac.datarain.model.POIIconData;
import com.turkcell.bipsaac.datarain.model.POIIconModel;
import com.turkcell.bipsaac.datarain.model.POIIconRequest;
import com.turkcell.bipsaac.datarain.model.User;

import model.Hero;
import tr.com.kasim.Service;

@RestController
public class GreetingController {

	private List<Hero> heroList;
	private static final String template = "Hello, %s!";
	private final AtomicLong counter = new AtomicLong();
	private static final List<User> userList;

	static {
		userList = new ArrayList<>();
	}

	public class Response {
		private boolean success = true;
		private String token = "this is a token";

		public boolean isSuccess() {
			return success;
		}

		public void setSuccess(boolean success) {
			this.success = success;
		}

		public String getToken() {
			return token;
		}

		public void setToken(String token) {
			this.token = token;
		}
		
	}

	@CrossOrigin
	@RequestMapping(method = RequestMethod.POST, path = "/login", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> login(@RequestBody User user) {
		
		new Service().test();

		return new ResponseEntity<>(new Response(), HttpStatus.OK);
	}

	@CrossOrigin
	@RequestMapping(method = RequestMethod.POST, path = "/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> create(@RequestBody User user) {
		if (user == null || StringUtils.isEmpty(user.getUsername()) || StringUtils.isEmpty(user.getPassword())) {
			return new ResponseEntity<>("unexpected user creditentals", HttpStatus.BAD_REQUEST);
		}
		user.setToken(UUID.randomUUID().toString());
		userList.add(user);
		return new ResponseEntity<>(user, HttpStatus.OK);
	}

	@CrossOrigin(origins = "http://localhost:8080")
	@RequestMapping(method = RequestMethod.GET, path = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<User>> users() {
		System.out.println("########################");
		for (User u : userList) {
			System.out.println(u.getUsername());
		}
		System.out.println("########################");
		return new ResponseEntity<>(userList, HttpStatus.OK);
	}

	@CrossOrigin
	@RequestMapping(method = RequestMethod.POST, path = "/authenticate", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> authenticate(@RequestBody LoginInput input) {
		System.out.println("#####################################");

		if (input == null) {
			return new ResponseEntity<>("unexpected input", HttpStatus.BAD_REQUEST);
		}

		System.out.println("Login Request by: " + input.getUsername());

		User user = getUser(input.getUsername());

		if (user.getPassword().equals(input.getPassword())) {
			System.out.println("#####################################");
			return new ResponseEntity<>(user, HttpStatus.OK);
		}

		System.out.println("#####################################");
		return new ResponseEntity<>("{ \"error\": \"invalid user credentials\"}", HttpStatus.BAD_REQUEST);
	}

	private User getUser(String username) {
		if (username == null)
			return new User();
		for (User user : userList) {
			if (user != null && username.equals(user.getUsername())) {
				return user;
			}
		}
		return new User();
	}

	@RequestMapping("/greeting")
	public Greeting greeting(@RequestParam(value = "name", defaultValue = "World") String name) {
		return new Greeting(counter.incrementAndGet(), String.format(template, name));
	}

	@CrossOrigin
	@RequestMapping(method = RequestMethod.POST, path = "/addHero")
	public Hero addHero(@RequestBody final Hero hero) {
		if (hero == null) {
			System.out.println("Null Input Recieved");
		} else {
			getHeroList().add(hero);
			System.out.println("Hero name is " + hero.getName());
		}

		return hero;
	}

	@CrossOrigin
	@RequestMapping(method = RequestMethod.GET, path = "/getHeroes", produces = MediaType.APPLICATION_JSON_VALUE)
	public Hero[] getHeroes() {
		// public ResponseEntity<List<Hero>> getHeroes() {
		System.out.println("####################################");
		for (Hero h : getHeroList()) {
			System.out.println(h.getName());
		}
		System.out.println("####################################");
		// return new ResponseEntity<List<Hero>>(getHeroList(), HttpStatus.OK);
		return getHeroList().toArray(new Hero[getHeroList().size()]);
	}

	public List<Hero> getHeroList() {
		if (heroList == null) {
			heroList = new ArrayList<Hero>();
		}
		return heroList;
	}
	
	@CrossOrigin
	@RequestMapping(method = RequestMethod.POST, path = "/getPOIIcon", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getPOIIcon(@RequestBody POIIconRequest req) {
		List<POIIconModel> resultList = null;
		POIIconData data = new POIIconData();
		
		if ("ICN".equals(req.getType())) {
			resultList = data.getIcons();
		} else if ("THMB".equals(req.getType())) {
			resultList = data.getThumbnails();
		} else if ("ALL".equals(req.getType())){
			resultList = new ArrayList<>();
			resultList.addAll(data.getIcons());
			resultList.addAll(data.getThumbnails());
		}
		System.out.println("################################################################################################");
		System.out.println(req);
		System.out.println("-------------------------------------------------------------------------------------------------");
		for (POIIconModel poiIconModel : resultList) {
			System.out.println(poiIconModel.toString());
		}
		System.out.println("################################################################################################");
		
		return new ResponseEntity<>(resultList, HttpStatus.OK);
	}
}
